package main

import (
	"fmt"
	"runtime"
	"strings"
)

func sourceMaker(toFilterChan chan<- string) {
	arrayString := []string{"гаф ad 1", "ваф da 11", "ваф a 1", "фав a 1", "баф"}
	for _, v := range arrayString {
		toFilterChan <- v
	}
	close(toFilterChan)
}

func splitSentences(toFilterChan <-chan string, filteredChan chan<- string) {
	for v := range toFilterChan {
		arrayOfStrings := strings.Split(v, " ")
		for _, str := range arrayOfStrings {
			filteredChan <- str
		}
	}
	close(filteredChan)
}

func removeDuplicate(toFilterChan <-chan string, filteredChan chan<- string) {
	listOfStrings := make(map[string]bool)
	for chanElement := range toFilterChan {
		if !listOfStrings[chanElement] {
			listOfStrings[chanElement] = true
			filteredChan <- chanElement
		}
	}
	close(filteredChan)
}

func printFilteredWords(filteredChan <-chan string) {
	for v := range filteredChan {
		fmt.Printf("| %v |", v)
	}
}

func printCountGoroutines() {
	fmt.Printf("\nCount of goroutines: %v\n", runtime.NumGoroutine())
}

func main() {
	filteredChan := make(chan string)
	toFilterChan := make(chan string, 1)

	go sourceMaker(toFilterChan)
	//	go removeDuplicate(toFilterChan, filteredChan)
	go splitSentences(toFilterChan, filteredChan)

	printCountGoroutines()
	printFilteredWords(filteredChan)
	printCountGoroutines()
}
